import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;


public class ButtonGame extends JFrame implements ActionListener, Serializable {
    private int[][] gameState = {{1, 2, 3}, {4, 5, 6}, {7, 8, -1}};
    private JButtonX[] buttonElems;
    private ButtonGame currentGame;

    private void remakeGame() {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
//                System.out.print(currentGame.gameState[i][j] + " ");
                gameState[i][j] = currentGame.gameState[i][j];
                buttonElems[i * 3 + j].setText("" + currentGame.gameState[i][j]);
                if (currentGame.gameState[i][j] == -1) buttonElems[i * 3 + j].setVisible(false);
                else buttonElems[i * 3 + j].setVisible(true);
            }
            System.out.println();
        }

        revalidate();
        pack();
    }

    private ButtonGame() {
        currentGame = this;
        setTitle("8 Puzzle");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        setLayout(new BorderLayout());
//        setResizable(false);
        JMenuBar mb = new JMenuBar();
        JMenu jm = new JMenu("Game Options");
        JMenuItem open = new JMenuItem("Open");
        open.addActionListener(e -> {
            try {
                FileInputStream fos = new FileInputStream("gameState");
                ObjectInputStream oos = new ObjectInputStream(fos);
                currentGame = (ButtonGame) oos.readObject();
                remakeGame();
            } catch (IOException | ClassNotFoundException e1) {
                JOptionPane.showMessageDialog(null, "Error opening file!");
                e1.printStackTrace();
            }

        });
        JMenuItem save = new JMenuItem("Save");
        save.addActionListener(e -> {
            try {
                FileOutputStream fos = new FileOutputStream("gameState");
                ObjectOutputStream oos = new ObjectOutputStream(fos);
                oos.writeObject(currentGame);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            JOptionPane.showMessageDialog(null, "Saved!");
        });
        jm.add(save);
        jm.add(open);
        mb.add(jm);
        setJMenuBar(mb);

        JPanel mainPanel = new JPanel(new GridLayout(3, 3, 1, 1));
        buttonElems = new JButtonX[10];
        for (int i = 0; i < 3; ++i) {
//			JPanel oneButton = new JPanel(new FlowLayout());
            for (int j = 0; j < 3; ++j) {
                JButtonX thebutton = new JButtonX("" + (i * 3 + j + 1), i, j);
                thebutton.addActionListener(this);
                buttonElems[i * 3 + j] = thebutton;
                //			oneButton.add(thebutton);
                mainPanel.add(thebutton);
            }
        }
        buttonElems[8].setVisible(false);
        add(new JLabel(" ", JLabel.CENTER), BorderLayout.NORTH);
        add(new JLabel(" "), BorderLayout.EAST);
        add(new JLabel(" "), BorderLayout.WEST);
        add(mainPanel, BorderLayout.CENTER);
        add(new JLabel(" "), BorderLayout.SOUTH);
//        setExtendedState(MAXIMIZED_BOTH);
//        pack();
        setSize(230, 230);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(ButtonGame::new);
    }

    private boolean isValid(int row, int col) {
        return row < 3 && row >= 0 && col < 3 && col >= 0;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButtonX clicked = (JButtonX) e.getSource();
        int r = clicked.getRow();
        int c = clicked.getCol();

        // Check if current tile can be moved down,right,up,left respectively

        if (isValid(r + 1, c) && gameState[r + 1][c] == -1) {
            clicked.setVisible(false);
            gameState[r + 1][c] = gameState[r][c];
            gameState[r][c] = -1;
            buttonElems[(r + 1) * 3 + c].setText("" + gameState[r + 1][c]);
            buttonElems[(r + 1) * 3 + c].setVisible(true);
        } else if (isValid(r, c + 1) && gameState[r][c + 1] == -1) {
            clicked.setVisible(false);
            gameState[r][c + 1] = gameState[r][c];
            gameState[r][c] = -1;
            buttonElems[r * 3 + c + 1].setText("" + gameState[r][c + 1]);
            buttonElems[r * 3 + c + 1].setVisible(true);
        } else if (isValid(r - 1, c) && gameState[r - 1][c] == -1) {
            clicked.setVisible(false);
            gameState[r - 1][c] = gameState[r][c];
            gameState[r][c] = -1;
            buttonElems[(r - 1) * 3 + c].setText("" + gameState[r - 1][c]);
            buttonElems[(r - 1) * 3 + c].setVisible(true);
        } else if (isValid(r, c - 1) && gameState[r][c - 1] == -1) {
            clicked.setVisible(false);
            gameState[r][c - 1] = gameState[r][c];
            gameState[r][c] = -1;
            buttonElems[r * 3 + c - 1].setText("" + gameState[r][c - 1]);
            buttonElems[r * 3 + c - 1].setVisible(true);
        }
    }
}
