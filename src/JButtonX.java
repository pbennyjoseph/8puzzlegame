import javax.swing.*;


class JButtonX extends JButton {
    private int row, col;

    JButtonX(String x) {
        super(x);
        setFocusable(false);
        this.row = -1;
        this.col = -1;
    }

    JButtonX(String x, int row, int col) {
        super(x);
        setFocusable(false);
        this.row = row;
        this.col = col;
    }

    JButtonX() {
        setFocusable(false);
        this.row = -1;
        this.col = -1;
    }

    int getRow() {
        return this.row;
    }

    int getCol() {
        return this.col;
    }
}
